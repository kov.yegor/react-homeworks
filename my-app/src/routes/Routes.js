import React from 'react';

import {Routes, Route} from "react-router-dom";
import HomePage from '../components/HomePage/HomePage';
import CartPage from '../components/CartPage/CartPage';
import FavPage from '../components/FavouritesPage/FavouritesPage'

const AppRoutes = (props) => {
    const {  items, addToCart,setFavorite, removeFromCart, cartItem, setCartItem, deleteFromFav} = props
    return (
        <Routes>
            <Route path='/' element={<HomePage  items={items} addToCart={addToCart} setFavorite={setFavorite}/>}/>
            <Route path='/cart' element={<CartPage cartItem={cartItem} removeFromCart={removeFromCart} setCartItem={setCartItem} />}/>
            <Route path='/fav' element={<FavPage setFavorite={setFavorite} deleteFromFav={deleteFromFav} />}/>
        </Routes>
    )
}
export default AppRoutes;