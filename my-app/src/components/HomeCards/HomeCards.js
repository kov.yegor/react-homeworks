import React from "react";
import "./HomeCards.scss";
import CardList from "../CardList/CardList";

const HomeCards = (props) => {
  const { items, addToCart, openModal, closeModal, setFavorite } = props;
  return (
    <div className="container">
    <CardList items={items} addToCart={addToCart} openModal={openModal} closeModal={closeModal} setFavorite={setFavorite}  />
    </div>
  );
};

export default HomeCards;

// class HomeCards extends React.Component {

//     render() {
//         const {items, addToCart, openModal, closeModal,setFavorite} = this.props;
//         return (
//             <div className="container">
//             <CardList items={items} addToCart={addToCart} openModal={openModal} closeModal={closeModal} setFavorite={setFavorite}  />
//             </div>
//         )
//     }
// }

