import React from "react";
import "./Modal.scss"

const Modal = ({closeModal})=> {

return <div className="modalContainer">
    
    <div className="modalBackground" onClick={()=>closeModal(false)}/>
    <div className="content">
    <div className="titleCloseBtn">
        <button onClick={()=>closeModal(false)}>X</button>
        </div>
        <div className="title">

            <h1>Add to cart</h1>
        </div>
        <div className="body">
            <p>
                Are u sure u want to add it to your cart
            </p>
        </div>
        <div className="footer">
            <button>Ok</button>
            <button onClick={()=>closeModal(false)} id='cancelButton'>Cancel</button>
        </div>

        </div>
    
</div>
}




export default Modal;
