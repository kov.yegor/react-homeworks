import React from 'react';
import isFavContainer from "../isFavContainer/isFavContainer"
const FavouritesPage = (props) => {
const {setFavorite, removeFavourite} = props;
    return (
        <>
<isFavContainer setFavorite={setFavorite} removeFavourite={removeFavourite} />
        </>
    )
}
export default FavouritesPage;