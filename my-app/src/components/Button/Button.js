import React from "react";
import "./Button.scss"
class Button extends React.Component{
    render(){
const {handleClick, text} = this.props

        return(
            <button className ="button" onClick={handleClick}>{text}</button>
        )
    }
}

export default Button;      