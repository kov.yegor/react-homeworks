import React, { Component } from "react";
import Card from "../Card/Card";
import "./CardList.scss"


const CardList = (props)=>{
  const {items,addToCart,setFavorite} = props;
  return (
    <section className="cardList">
      <div className="container">
        {items && items.map(item=><Card key={item.id}{...item} addToCart={addToCart} setFavorite={setFavorite} />)}
      </div>

    </section>
  )
}










// class CardList extends React.Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//       clothes: [],
//       openModal:false,
//     };
//   }
 
//   componentDidMount() {
//     fetch("./store.json")
//       .then((res) => res.json())
//       .then((data) => {
//         this.setState({ clothes: data });
//       });
//   }
// // showModal=()=>{
// //     this.setState({openModal:true});
// //     console.log(this.state.openModal);
// // }
// // closeModal=()=>{
// //     this.setState({openModal:false});
// // }
//   render() {
//     const cardArr = this.state.clothes.map((item) => {
//         return (
            
//         <div key={item.article}>
//           <Card
//             isFavourite={item.isFavourite}
//             name={item.name}
//             price={item.price}
//             img={item.img}
//             color={item.color}
 
//           />
//           {this.state.openModal && <Modal text="" handleClick={this.closeModal}/>}
//         </div>
//       );
//     });
//     return <div>{cardArr}</div>;
//   }
// }
// CardList.propTypes = {
//   clothes: PropTypes.array
  // openModal: PropTypes.bool,
// }
// CardList.defaultProps = {
//   clothes: [],

// }
export default CardList;
