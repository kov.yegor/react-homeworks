import React from 'react';
import "./isFavContainer.scss";
import {getItemFromLS} from '../../utils/api/localStorage'
import isFavItem from "../isFavItem/isFavItem"
const isFavContainer = (props) => {
    const {setFavorite, removeFavourite} = props;
const favData = getItemFromLS('favItems')
    return (
        <section className="root">
         
            <div className="isFavContainer">
            {!favData ? <p>Add some products</p>
             : favData.map((elem)=>
             <isFavItem count={elem.count} text={elem.name} article={elem.article} img={elem.img} key={elem.article} setFavorite={setFavorite} removeFavourite={removeFavourite}/>)}
            </div>
        </section>
    )
}

export default isFavContainer;