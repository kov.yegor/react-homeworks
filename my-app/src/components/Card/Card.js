import React, { useState } from "react";
import Button from "../Button/Button";
import Modal from "../Modal/Modal"
import {ReactComponent as StarSvg} from "../../assets/Star/star-plus.svg";
import {ReactComponent as StarDelete} from "../../assets/Star/star-delete.svg";

import "./Card.scss";

const Card = (props) => {
  const [openModal, setOpenModal] = useState(false);

  const { name, price,article, img, addToCart,isFavorite,closeModal, setFavorite } = props;

  return (
      <div className='mainContent'>
          <div className="fav" onClick={()=>setFavorite (article)}>
          {isFavorite ? <StarDelete/> : <StarSvg/>}
          </div>
          <p>{ name }</p>
          <span>{price}</span>
          <img src={img} alt={name} />
          {openModal && <Modal closeModal={setOpenModal} />}
          <Button onClick={()=>{setOpenModal(true)}} handleClick={()=>addToCart(article)} text='Add to cart'/>
        

      </div>
  )

  // const [openModal, setOpenModal] = useState(false);

  // const { name, price, img, article, color,isFavourite } = props;

  // return (
  //   <>
  //     <div className="card">
  //       <div className="cardGrid" key={article}>
  //       {isFavourite ? <StarDelete/> : <StarSvg/>}

  //         <h1>{name}</h1>
  //         <h2>{price}</h2>
  //         <img className="imgSize" src={img} />
  //         <p>{article}</p>
  //         <p>{color}</p>
  //         {openModal && <Modal closeModal={setOpenModal} />}
  //         <button
  //           className="openBtn"
  //           onClick={() => {
  //             setOpenModal(true);
  //           }}
  //         >
  //           Add to cart
  //         </button>
  //       </div>
  //     </div>
  //   </>
  // );
};
// class Card extends React.Component {

//     constructor(props){
//         super(props)

//     }

//     render() {

//         const {name,price,img,article,color,openModal} = this.props;
//         return(
//             <>
//             <div className="card">
//             <div className="cardGrid" key={article}>
//             {starSvg()}
//             {starDelete()}
//                 <h1>{name}</h1>
//                 <h2>{price}</h2>
//                 <img className="imgSize" src={img}/>
//                 <p>{article}</p>
//                 <p>{color}</p>
//                 <button className="addToCartBtn" onClick={openModal} >Add to cart</button>
//              </div>
//             </div>
//             </>
//         )
//     }
// }

export default Card;
