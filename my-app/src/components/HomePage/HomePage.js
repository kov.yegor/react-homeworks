import React from 'react';
import HomeCards from '../../components/HomeCards/HomeCards'


const HomePage = (props) => {
const {  items, addToCart,setFavorite} = props

    return (
        <>
<div className="homePageDiv">
 <HomeCards  items={items} addToCart={addToCart} setFavorite={setFavorite} />
 </div>
        </>
    )
}
export default HomePage;