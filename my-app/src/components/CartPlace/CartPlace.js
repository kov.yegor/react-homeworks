import React from "react";
import "./CartPlace.scss"

const CartPlace =(props)=>{
const {setCartItem,removeFromCart} = props
const mapCart = getFromLS('addToCartItem')
return (
<section className="cartPlaceSection">
    <div className="cartGrid">{mapCart
     && mapCart.map((elem)=><Cart setCartItem={setCartItem} count={elem.count} title={elem.name} article={elem.article} img={elem.img} key={elem.article} removeFromCart={removeFromCart} />)}
</div>
</section>
)
}
export default CartPlace;