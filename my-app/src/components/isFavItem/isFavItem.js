import React from 'react';
import "./isFavItem.scss";
import Button from '../Button/Button'

const isFavItem = (props) => {
    const {price, text, img,  removeFavourite} = props

    return (
        <>

        <div >
            <div >
                <div >
                    <img src={img} alt={text}/>
                </div>
                <span >{text}</span>
                <span >{price}</span>
                <Button text='-' handleClick={removeFavourite}/>
            </div>
        </div>
            </>
    )
}

export default isFavItem;