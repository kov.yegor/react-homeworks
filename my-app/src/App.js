import React,{useState,useEffect}  from "react";
import Header from "./components/Header/Header"
import "./App.css";
import HomeCards from "./components/HomeCards/HomeCards"
import Modal from "./components/Modal/Modal";
import PropTypes from "prop-types"
import MainRoutes from "./routes/Routes";



const App =()=>{


const [items,setItems] = useState([])
const [addToCartItem,setAddToCartItem] = useState([])
const [isFavItems,setIsFavItems] = useState([])


const cardsFetch = async ()=>{
  const store = await fetch("./products.json").then((store) =>
   store.json());


   setItems(store);
}

useEffect(() => {
  cardsFetch();
}, []);


const removeFavourite = ()=>{
  setIsFavItems((favItems)=>{
    const favItemsLS = localStorage.getItem("favItems");
    if (favItemsLS) {
      localStorage.removeItem(`favItems`);
    }
    const newFavItem = favItems;
    return newFavItem.favItems;
   })
}

// useEffect( async ()=>{
//   const data = await fetch('./store.json')
//   .then(data => 
//     data.json()); 
// setItems(data);
// }) 
//   const data = await fetch('./store.json')
//   .then(data => 
//     data.json());
//   console.log(data);
// this.setState({items: data});

const addToCart = (name, price, article, img) => {
  setAddToCartItem((state) => {
    const index = state.findIndex((item) => item.article === article);
    if (index === -1) {
      const newCartItem = {
        state: [...state, { name, price, article, img, count: 1 }],
      };
      const jsonCartItem = JSON.stringify(newCartItem.state);
      if (jsonCartItem) {
        localStorage.setItem("addToCartItem", jsonCartItem);
      }
      return newCartItem.state;
    } else {
      const newCartItem = state;
      newCartItem[index].count += 1;
      const jsonCartItem = JSON.stringify(newCartItem);
      localStorage.setItem("addToCartItem", jsonCartItem);
      return addToCartItem;
    }
  });
};


    const addToFavorite = (name, price,article, img) => {
      setIsFavItems((favItems) => {
        const index = favItems.findIndex((items) => items.article === article);
        if (index === -1) {
          const newFavItem = {
            favItems: [...favItems, { name, price, article, img, isFavorite: true }],
          };
          console.log(newFavItem.favItems);
          const jsonFavItem = JSON.stringify(newFavItem.favItems);
          if (jsonFavItem) {
            localStorage.setItem("favItems", jsonFavItem);
          }
          return newFavItem.favItems;
        } else {
          const favItemsLS = localStorage.getItem("favItems");
          if (favItemsLS) {
            localStorage.removeItem(`favItems`);
          }
          const newFavItem = favItems;
          return newFavItem.favItems;
        }
      });
    };
  //  const addToFavorite = (article) => {
  //   this.setState( state => {
  //       const searchIndex = state.isFavItems.findIndex(items => items.article === article)
  //       if(searchIndex   === -1){
  //          const newFavItem = {...state, isFavItems: [...state.isFavItems, {article, isFavorite: true}]}
         
  //          state.isFavItems = newFavItem
          
  //        console.log(newFavItem);
  //         const jsonFavItem = JSON.stringify(newFavItem.isFavItems)
        
        
  //         if(jsonFavItem) {localStorage.setItem('isFavItems', jsonFavItem)}
  //          return state.isFavItems
  
  //       } 
  //       else{
  //    const isFavItems = localStorage.getItem('isFavItems')
         
  //         if (isFavItems) {
  //          localStorage.removeItem(`isFavItems`)
           
  //         }
  //       const newFavItem = {...state}
      
  //       return newFavItem
  //       }
  //   } ) 
  // }
  // const { items } = this.state;
  return (
    <>
    <Header/>
    <MainRoutes items={items} setAddToCartItem={setAddToCartItem} addToCartItem={addToCartItem}  removeFavourite={ removeFavourite } addToCart={addToCart}  />
    <HomeCards addToCart={addToCart} addToCartItem={addToCartItem} setFavorite={addToFavorite} items={items}/>
    </>
  )


}



export default App;


